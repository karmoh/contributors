package com.github.contributors.config.error;

public class WebException extends RuntimeException {

    private static final long serialVersionUID = -8375655612540699875L;
    private final String messageText;

    public WebException(String messageText) {
        this.messageText = messageText;
    }

    public String getMessageText() {
        return messageText;
    }
}
