package com.github.contributors.config.error;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;

import java.lang.invoke.MethodHandles;
import java.time.Clock;
import java.time.LocalDateTime;
import java.util.UUID;

@ControllerAdvice
@RequestMapping(produces = "application/json")
public class ErrorControllerAdvice {
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @ExceptionHandler(WebException.class)
    public ResponseEntity<ErrorMessage> webException(final WebException e) {
        ErrorMessage responseMessage = new ErrorMessage(UUID.randomUUID().toString(), e.getMessageText(), LocalDateTime.now(Clock.systemUTC()));
        return new ResponseEntity<>(responseMessage, HttpStatus.TOO_MANY_REQUESTS);
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity<ErrorMessage> technicalException(final Exception exception) {
        String errorId = UUID.randomUUID().toString();
        ErrorMessage responseMessage = new ErrorMessage(errorId, "Technical exception ref:" + errorId, LocalDateTime.now(Clock.systemUTC()));
        LOGGER.error(responseMessage.toString(), exception);
        return new ResponseEntity<>(responseMessage, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
