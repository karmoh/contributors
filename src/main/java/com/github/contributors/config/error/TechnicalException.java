package com.github.contributors.config.error;

public class TechnicalException extends RuntimeException {
    private static final long serialVersionUID = 2810051746132938456L;

    public TechnicalException(Throwable cause) {
        super(cause);
    }

}
