package com.github.contributors.config.error;


import lombok.*;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
class ErrorMessage {
    private String ref;
    private String errorMessage;
    private LocalDateTime dateTime;

}
