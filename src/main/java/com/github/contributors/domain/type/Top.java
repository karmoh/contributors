package com.github.contributors.domain.type;

public enum Top {
    TOP50,
    TOP100,
    TOP150
}
