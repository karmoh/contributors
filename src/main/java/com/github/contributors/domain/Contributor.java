package com.github.contributors.domain;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Contributor {
    private String name;
    private String profileUrl;
}
