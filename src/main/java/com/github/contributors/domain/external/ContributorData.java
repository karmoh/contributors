package com.github.contributors.domain.external;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ContributorData {

    @JsonProperty("login")
    private String login;
    @JsonProperty("html_url")
    private String htmlUrl;


}