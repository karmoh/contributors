package com.github.contributors.service;

import com.github.contributors.domain.Contributor;
import com.github.contributors.domain.external.ContributorData;
import com.github.contributors.domain.type.Top;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Log4j2
public class ContributorsService {

    private final GithubService githubService;

    @Cacheable(value = "getTopContributorsForCity")
    public List<Contributor> getTopContributorsForCity(String city, Top top) {
        log.info("getting ({}) top contributors for {}", top, city);
        List<ContributorData> topContributorsContributorDataList = githubService.requestTopContributorsForCity(city, 1);
        if (Top.TOP150.equals(top) && topContributorsContributorDataList.size() == 100) {
            topContributorsContributorDataList.addAll(githubService.requestTopContributorsForCity(city, 2));
        }
        if (Top.TOP50.equals(top) && topContributorsContributorDataList.size() > 50) {
            return mapItemListToContributorList(topContributorsContributorDataList.subList(0, 50));
        }
        return mapItemListToContributorList(topContributorsContributorDataList);
    }

    private List<Contributor> mapItemListToContributorList(List<ContributorData> contributorData) {
        if (CollectionUtils.isEmpty(contributorData)) {
            return Collections.emptyList();
        }
        return contributorData.stream().map(this::mapItemToContributor).collect(Collectors.toList());
    }

    private Contributor mapItemToContributor(ContributorData contributorData) {
        return Contributor.builder()
                .name(contributorData.getLogin())
                .profileUrl(contributorData.getHtmlUrl())
                .build();
    }
}
