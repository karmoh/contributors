package com.github.contributors.service;

import com.github.contributors.config.error.TechnicalException;
import com.github.contributors.config.error.WebException;
import com.github.contributors.domain.external.ContributorData;
import com.github.contributors.domain.external.ContributorsGet;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;

@Service
@Log4j2
public class GithubService {
    private static final String SCHEME = "https";
    private static final String HOST = "api.github.com";
    private static final String PATH = "/search/users";
    private final RestTemplate restTemplate;
    private final String userName;
    private final String token;
    private final Clock clock;

    public GithubService(RestTemplate restTemplate,
                         @Value("${github.userName:#{null}}") String userName,
                         @Value("${github.token:#{null}}") String token,
                         Clock clock) {
        this.restTemplate = restTemplate;
        this.userName = userName;
        this.token = token;
        this.clock = clock;
    }

    @Cacheable(value = "requestTopContributorsForCity")
    public List<ContributorData> requestTopContributorsForCity(String city, Integer page) {
        log.info("requesting  page {} top contributors for {}", page, city);
        ResponseEntity<ContributorsGet> contributorsGetResponse;
        try {
            contributorsGetResponse = restTemplate.exchange(getRequestUrl(city, page), HttpMethod.GET, createHttpEntity(), ContributorsGet.class);
        } catch (HttpClientErrorException httpClientErrorException) {
            if (httpClientErrorException.getStatusCode().equals(HttpStatus.FORBIDDEN)) {
                throw new WebException("Too many request done to external api, wait " + getRetryInSecondsFromHttpHeaders(httpClientErrorException.getResponseHeaders()) + " seconds");
            } else {
                throw new TechnicalException(httpClientErrorException);
            }
        }
        if (contributorsGetResponse.getBody() == null || CollectionUtils.isEmpty(contributorsGetResponse.getBody().getItems())) {
            return Collections.emptyList();
        }
        return contributorsGetResponse.getBody().getItems();
    }

    private long getRetryInSecondsFromHttpHeaders(HttpHeaders responseHeaders) {
        if (responseHeaders != null) {
            List<String> rateLimitResetHeaderList = responseHeaders.get("X-RateLimit-Reset");
            if (!CollectionUtils.isEmpty(rateLimitResetHeaderList)) {
                LocalDateTime resetDateTime = epochToLocalDateTime(rateLimitResetHeaderList.get(0));
                if (resetDateTime != null) {
                    return ChronoUnit.SECONDS.between(LocalDateTime.now(clock), resetDateTime);
                }
            }
        }
        return 60;
    }

    private LocalDateTime epochToLocalDateTime(String epochDateTime) {
        if (epochDateTime == null) {
            return null;
        }
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(Long.parseLong(epochDateTime) * 1000), ZoneId.of("UTC"));
    }

    private String getRequestUrl(String city, Integer page) {
        UriComponents builder = UriComponentsBuilder.newInstance()
                .scheme(SCHEME).host(HOST).path(PATH)
                .queryParam("q", "location:" + city)
                .queryParam("page", page)
                .queryParam("per_page", page == 2 ? 50 : 100)
                .queryParam("sort", "repositories")
                .build();
        return builder.toUriString();
    }

    private HttpEntity createHttpEntity() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        if (userName != null && token != null) {
            log.info("setting token auth for {}", userName);
            httpHeaders.setBasicAuth(userName, token);
        } else {
            log.warn("user and token are not set up");
        }
        return new HttpEntity<>(httpHeaders);
    }
}
