package com.github.contributors.web;

import lombok.Data;

@Data
public class ServiceResponse<T> {

    private T data;
    private Integer total;

    static <T> ServiceResponse<T> ok(T data, Integer total) {
        ServiceResponse<T> response = new ServiceResponse<>();
        response.setData(data);
        response.setTotal(total);
        return response;
    }

}
