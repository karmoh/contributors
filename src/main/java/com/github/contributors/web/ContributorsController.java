package com.github.contributors.web;

import com.github.contributors.domain.Contributor;
import com.github.contributors.domain.type.Top;
import com.github.contributors.service.ContributorsService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
public class ContributorsController {

    private final ContributorsService contributorsService;

    @GetMapping("/top-contributors")
    public ServiceResponse<List<Contributor>> getTopContributorsForCity(
            @RequestParam String city,
            @RequestParam(required = false, defaultValue = "TOP50") Top top) {
        List<Contributor> result = contributorsService.getTopContributorsForCity(city.toLowerCase(), top);
        return ServiceResponse.ok(result, result.size());
    }
}
