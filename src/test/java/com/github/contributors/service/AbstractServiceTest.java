package com.github.contributors.service;

import org.junit.jupiter.api.BeforeEach;
import org.mockito.MockitoAnnotations;

import java.time.Clock;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;

public abstract class AbstractServiceTest<T> {
    private static final LocalDate FIXED_DATE = LocalDate.of(2019, 11, 9);
    public static final Clock FIXED_CLOCK = Clock.fixed(
            FIXED_DATE.atStartOfDay().toInstant(ZoneOffset.UTC), ZoneId.of("UTC"));


    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

    }

    protected abstract T createService();
}
