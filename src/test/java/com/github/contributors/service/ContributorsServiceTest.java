package com.github.contributors.service;

import com.github.contributors.domain.Contributor;
import com.github.contributors.domain.external.ContributorData;
import com.github.contributors.domain.type.Top;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

class ContributorsServiceTest extends AbstractServiceTest<ContributorsService> {

    @Mock
    private GithubService githubService;

    @Test
    void getTopContributorsForCity50PerPage() {
        List<ContributorData> contributorData = new ArrayList<>();
        for (int i = 0; i < 55; i++) {
            contributorData.add(ContributorData.builder().login("login" + i).htmlUrl("htmlUrl" + i).build());
        }
        when(githubService.requestTopContributorsForCity("city", 1)).thenReturn(contributorData);
        List<Contributor> result = createService().getTopContributorsForCity("city", Top.TOP50);
        verify(githubService).requestTopContributorsForCity(anyString(), anyInt());
        assertEquals(50, result.size());
        Contributor contributor = result.get(0);
        assertEquals("login0", contributor.getName());
        assertEquals("htmlUrl0", contributor.getProfileUrl());
    }

    @Test
    void getTopContributorsForCity100perPage() {
        List<ContributorData> contributorData = new ArrayList<>();
        for (int i = 0; i < 55; i++) {
            contributorData.add(ContributorData.builder().login("login" + i).htmlUrl("htmlUrl" + i).build());
        }
        when(githubService.requestTopContributorsForCity("city", 1)).thenReturn(contributorData);
        List<Contributor> result = createService().getTopContributorsForCity("city", Top.TOP100);
        verify(githubService).requestTopContributorsForCity(anyString(), anyInt());
        assertEquals(55, result.size());
        Contributor contributor = result.get(0);
        assertEquals("login0", contributor.getName());
        assertEquals("htmlUrl0", contributor.getProfileUrl());
    }

    @Test
    void getTopContributorsForCity150perPage() {
        List<ContributorData> contributorData = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            contributorData.add(ContributorData.builder().login("login" + i).htmlUrl("htmlUrl" + i).build());
        }
        List<ContributorData> itemsPage2 = new ArrayList<>();
        for (int i = 0; i < 50; i++) {
            itemsPage2.add(ContributorData.builder().login("login" + i).htmlUrl("htmlUrl" + i).build());
        }
        when(githubService.requestTopContributorsForCity("city", 1)).thenReturn(contributorData);
        when(githubService.requestTopContributorsForCity("city", 2)).thenReturn(itemsPage2);
        List<Contributor> result = createService().getTopContributorsForCity("city", Top.TOP150);
        verify(githubService, times(2)).requestTopContributorsForCity(anyString(), anyInt());
        assertEquals(150, result.size());
        Contributor contributor = result.get(0);
        assertEquals("login0", contributor.getName());
        assertEquals("htmlUrl0", contributor.getProfileUrl());
    }

    @Test
    void getTopContributorsForCityEmptyResult() {
        when(githubService.requestTopContributorsForCity("city", 1)).thenReturn(Collections.emptyList());
        List<Contributor> result = createService().getTopContributorsForCity("city", Top.TOP150);
        verify(githubService).requestTopContributorsForCity(anyString(), anyInt());
        assertTrue(result.isEmpty());
    }

    @Override
    protected ContributorsService createService() {
        return new ContributorsService(githubService);
    }
}