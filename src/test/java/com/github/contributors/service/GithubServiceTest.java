package com.github.contributors.service;

import com.github.contributors.config.error.WebException;
import com.github.contributors.domain.external.ContributorData;
import com.github.contributors.domain.external.ContributorsGet;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class GithubServiceTest extends AbstractServiceTest<GithubService> {

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private ContributorsGet contributorsGet;
    @Mock
    private ContributorData contributorData;

    @Test
    void requestTopContributorsForCity() {
        when(restTemplate.exchange(eq("https://api.github.com/search/users?q=location:city&page=1&per_page=100&sort=repositories"),
                eq(HttpMethod.GET), any(), eq(ContributorsGet.class))).thenReturn(new ResponseEntity<>(contributorsGet, HttpStatus.OK));
        when(contributorsGet.getItems()).thenReturn(Collections.singletonList(contributorData));
        List<ContributorData> result = createService().requestTopContributorsForCity("city", 1);
        assertFalse(result.isEmpty());
    }

    @Test
    void requestTopContributorsForCityRequestLimit() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.put("X-RateLimit-Reset", Collections.singletonList("1573257605"));
        when(restTemplate.exchange(eq("https://api.github.com/search/users?q=location:city&page=1&per_page=100&sort=repositories"),
                eq(HttpMethod.GET), any(), eq(ContributorsGet.class))).thenThrow(HttpClientErrorException.create(HttpStatus.FORBIDDEN, "status", httpHeaders, null, StandardCharsets.UTF_8));
        WebException webException = assertThrows(WebException.class, () -> createService().requestTopContributorsForCity("city", 1));
        assertEquals("Too many request done to external api, wait 5 seconds", webException.getMessageText());

    }

    @Override
    protected GithubService createService() {
        return new GithubService(restTemplate,
                "userName",
                "token",
                FIXED_CLOCK);
    }
}