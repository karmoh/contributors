package com.github.contributors.integration;

import com.github.contributors.domain.external.ContributorData;
import com.github.contributors.domain.external.ContributorsGet;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collections;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.startsWith;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

class ContributorsITest extends IntegrationTestBase {
    private static final String CONTRIBUTORS_PATH = "/top-contributors";

    @Mock
    private ContributorsGet contributorsGet;

    @Test
    void getTopContributorsForCity() {
        when(restTemplate.exchange(eq("https://api.github.com/search/users?q=location:city&page=1&per_page=100&sort=repositories"),
                eq(HttpMethod.GET), any(), eq(ContributorsGet.class))).thenReturn(new ResponseEntity<>(contributorsGet, HttpStatus.OK));
        when(contributorsGet.getItems()).thenReturn(Collections.singletonList(ContributorData.builder()
                .htmlUrl("htmlUrl")
                .login("login")
                .build()));
        RestAssuredMockMvc
                .given()
                .param("city", "city")
                .when()
                .get(CONTRIBUTORS_PATH)
                .then()
                .log().ifError()
                .statusCode(HttpStatus.OK.value())
                .body("total", equalTo(1))
                .body("data[0].profileUrl", equalTo("htmlUrl"))
                .body("data[0].name", equalTo("login"));
    }

    @Test
    void getTopContributorsForCityTooManyRequests() {
        HttpHeaders httpHeaders = new HttpHeaders();
        ZoneId zoneId = ZoneId.of("UTC"); // or: ZoneId.of("Europe/Oslo");
        httpHeaders.put("X-RateLimit-Reset", Collections.singletonList(String.valueOf(LocalDateTime.now(zoneId).plusSeconds(5).atZone(zoneId).toEpochSecond())));
        when(restTemplate.exchange(eq("https://api.github.com/search/users?q=location:city&page=1&per_page=100&sort=repositories"),
                eq(HttpMethod.GET), any(), eq(ContributorsGet.class))).thenThrow(HttpClientErrorException.create(HttpStatus.FORBIDDEN, "status", httpHeaders, null, StandardCharsets.UTF_8));

        RestAssuredMockMvc
                .given()
                .param("city", "city")
                .when()
                .get(CONTRIBUTORS_PATH)
                .then()
                .log().all()
                .statusCode(HttpStatus.TOO_MANY_REQUESTS.value())
                .body("errorMessage", startsWith("Too many request done to external api, wait "));
    }

}
