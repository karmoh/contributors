### Prerequisites
```
Java 11
```
### Configuration

`
github.userName
`
`
github.token
`

optional application properties to increase request limits to github api

token is github personal access tokens
### Running application

```
gradlew bootRun
```

Go to [Swagger](http://localhost:8080/swagger-ui.html) resources







### Running the tests

```
gradlew test
```

### Built With

* Java 11
* Spring Boot
* Lombok
* SpringFox
* JUnit
* Mockito
* Rest-assured
